rajput_to_artificer = {
	is_rajput_modifier = yes
	reinforce_cost_modifier = 0.5
	land_morale = -0.05
	drill_decay_modifier = 0.25

	land_maintenance_modifier = 0.5
	fire_damage = 0.1
}

technocracy_rajput_bonus = {
	is_rajput_modifier = yes
	reinforce_cost_modifier = -0.5
	infantry_fire = 0.1
}

artificer_research_bonus_brilliance_mil = {
	fire_damage = 0.25
	shock_damage = 0.25
}

artificer_research_bonus_brilliance_ind = {
	production_efficiency = 0.2
}

artificer_research_bonus_brilliance_society = {
	monthly_splendor = 1
	free_policy = 1
}

artificer_research_bonus_technomancy_mil = {
	land_morale = 0.1
	war_exhaustion = -0.05
}

artificer_research_bonus_technomancy_ind = {
	yearly_corruption = -0.1
	trade_efficiency = 0.1
}

artificer_research_bonus_technomancy_society = {
	all_power_cost = -0.05
	advisor_pool = 1
}

artificer_research_bonus_machinery_mil = {
	backrow_artillery_damage = 0.15
	global_regiment_cost = -0.1
}

artificer_research_bonus_machinery_ind = {
	global_trade_goods_size_modifier = 0.1
	global_tariffs = 0.15
}

artificer_research_bonus_machinery_society = {
	republican_tradition = 0.3
	legitimacy = 1
	devotion = 1
	horde_unity = 1
	min_autonomy_in_territories = -0.05
}

artifice_sparkdrive_rifles = {
	is_rajput_modifier = yes

	fire_damage = 0.33
	land_morale = -0.05	#sparkdrives are dangerous, and leave you out on the open after fired

	reinforce_cost_modifier = 0.5
	reinforce_speed = -0.025
}

artifice_exo_arms = {
	is_rajput_modifier = yes

	infantry_power = 0.10

	reinforce_cost_modifier = 0.5
	reinforce_speed = -0.025
}

artifice_vorpal_bullets = {
	is_rajput_modifier = yes

	fire_damage = 0.2

	reinforce_cost_modifier = 0.5
	reinforce_speed = -0.025
}

artifice_war_golems = {
	is_rajput_modifier = yes

	shock_damage = 0.25
	shock_damage_received = -0.25

	reinforce_cost_modifier = 0.5
	reinforce_speed = -0.5
}

artifice_wandlocks = {
	is_rajput_modifier = yes

	land_morale = 0.15

	reinforce_speed = -0.05
	reinforce_cost_modifier = 1
}

artifice_mechanim_soldiers = {
	is_rajput_modifier = yes

	fire_damage_received = -0.2
	land_morale = 0.5
	land_attrition = -0.5

	reinforce_speed = -0.075
	reinforce_cost_modifier = 1.5
	movement_speed = -0.25
	fire_damage = -0.25
}

artifice_personal_mageshields = {
	is_rajput_modifier = yes

	fire_damage_received = -0.4

	reinforce_speed = -0.075
	reinforce_cost_modifier = 1.5
}

artifice_portable_turrets = {
	is_rajput_modifier = yes

	shock_damage_received = -0.20

	drill_decay_modifier = 0.33	#its harder to drill with portable turrets
	reinforce_cost_modifier = 0.5
	reinforce_speed = -0.025
}